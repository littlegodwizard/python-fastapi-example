from starlette.requests import Request
from fastapi_jwt_auth.exceptions import AuthJWTException
from starlette.responses import JSONResponse


async def authentication_error_handler(_: Request, exc: AuthJWTException) -> JSONResponse:
    return JSONResponse({"errors": [exc.message]}, status_code=exc.status_code)