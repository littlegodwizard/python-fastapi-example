from fastapi import APIRouter, Body, Depends
from modules.datatable.datatable import Request
from services.user_balance_history import show_user_balance_history
from services.users import get_current_user
from fastapi_jwt_auth import AuthJWT

router = APIRouter()


@router.post("/histories")
async def show_balance_histories(authorization: AuthJWT = Depends(),
                                 request: Request = Body(..., embed=True)):
    current_user = await get_current_user(authorization)
    return await show_user_balance_history(request, current_user)
