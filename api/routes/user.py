from fastapi import APIRouter, Header, Body, Depends
from starlette.requests import Request as Req
from models.helpers.utils import RWModel, BSONObjectID
from fastapi_jwt_auth import AuthJWT
from services.users import get_current_user, transfer_process, top_up_process

router = APIRouter()


class Nominal(RWModel):
    nominal: float


class Transfer(Nominal):
    email_target: str


class NominalWithUser(Nominal):
    user_id: BSONObjectID
    username: str


class TransferWithUser(Transfer):
    user_id: BSONObjectID
    username: str


class TransferWithSender(Transfer):
    sender: str


class TopUpWithUser(Nominal):
    user: str


class TransferInResponse(RWModel):
    transfer: TransferWithSender
    success: bool


class TopUpInResponse(RWModel):
    top_up: TopUpWithUser
    success: bool


@router.get("/me")
async def user_me(authorization: AuthJWT = Depends()):
    authorization.jwt_required()
    return await get_current_user(authorization)


@router.post("/transfer")
async def user_transfer(
        req: Req,
        authorization: AuthJWT = Depends(),
        transfer: Transfer = Body(..., embed=True)):
    authorization.jwt_required()
    current_user = await get_current_user(authorization)
    transfer_with_user = TransferWithUser(username=current_user.username,
                                          user_id=BSONObjectID().validate(current_user.id),
                                          nominal=transfer.nominal,
                                          email_target=transfer.email_target)
    await transfer_process(req.headers["user-agent"], **transfer_with_user.dict())
    return TransferInResponse(
        transfer=TransferWithSender(
            nominal=transfer.nominal,
            sender=current_user.username,
            email_target=transfer.email_target
        ),
        success=True
    )


@router.post("/top-up")
async def user_top_up(
        req: Req,
        authorization: AuthJWT = Depends(),
        nominal: Nominal = Body(..., embed=True, alias="top-up")):
    authorization.jwt_required()
    current_user = await get_current_user(authorization)
    nominal_with_user = NominalWithUser(username=current_user.username,
                                        user_id=BSONObjectID().validate(current_user.id),
                                        nominal=nominal.nominal,
                                        )
    await top_up_process(req.headers["user-agent"], **nominal_with_user.dict())
    return TopUpInResponse(
        top_up=TopUpWithUser(
            nominal=nominal.nominal,
            user=current_user.username,
        ),
        success=True
    )
