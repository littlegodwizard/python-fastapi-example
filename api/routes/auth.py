from fastapi import APIRouter, Body, HTTPException, Depends
from starlette.status import HTTP_201_CREATED, HTTP_400_BAD_REQUEST
from starlette.requests import Request as Req
from db.errors import EntityDoesNotExist
from models.users import (
    UserInCreate,
    UserInLogin,
    UserInResponse,
    UserWithToken,
    UserDao)
from modules.authentication.jwt import redis_conn, settings
from resources import strings
from fastapi_jwt_auth import AuthJWT
from services.users import check_email_is_taken, check_username_is_taken, create_user

router = APIRouter()


@router.post("/login", response_model=UserInResponse, name="auth:login")
async def login(
        user_login: UserInLogin = Body(..., embed=True, alias="user"),
        authorize: AuthJWT = Depends()
) -> UserInResponse:
    wrong_login_error = HTTPException(
        status_code=HTTP_400_BAD_REQUEST,
        detail=strings.INCORRECT_LOGIN_INPUT,
    )

    try:
        user = await UserDao().get_user_by_email(email=user_login.email)
    except EntityDoesNotExist as existence_error:
        raise wrong_login_error from existence_error

    if not user.check_password(user_login.password):
        raise wrong_login_error

    token = authorize.create_access_token(subject=user.id)
    return UserInResponse(
        user=UserWithToken(
            id=user.id,
            username=user.username,
            email=user.email,
            token=token,
        ),
    )


@router.post(
    "",
    status_code=HTTP_201_CREATED,
    response_model=UserInResponse,
    name="auth:register",
)
async def register(
        req: Req,
        user_create: UserInCreate = Body(..., embed=True, alias="user"),
        authorize: AuthJWT = Depends()
) -> UserInResponse:
    if await check_username_is_taken(user_create.username):
        raise HTTPException(
            status_code=HTTP_400_BAD_REQUEST,
            detail=strings.USERNAME_TAKEN,
        )

    if await check_email_is_taken(user_create.email):
        raise HTTPException(
            status_code=HTTP_400_BAD_REQUEST,
            detail=strings.EMAIL_TAKEN,
        )

    user = await create_user(req.headers["user-agent"], **user_create.dict())

    token = authorize.create_access_token(subject=user.id)
    return UserInResponse(
        user=UserWithToken(
            id=user.id,
            username=user.username,
            email=user.email,
            token=token,
        ),
    )


@router.post("/logout", name="auth:logout")
async def logout(
        authorize: AuthJWT = Depends()
):
    authorize.jwt_required()
    jti = authorize.get_raw_jwt()['jti']
    # redis_conn.setex(jti, settings.refresh_expires, 'true')
    redis_conn.setex(jti, settings.access_expires, 'true')
    return {"msg": "logout success"}
