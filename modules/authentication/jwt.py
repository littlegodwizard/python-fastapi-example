from datetime import timedelta
from fastapi_jwt_auth import AuthJWT

from pydantic import BaseModel
from redis import Redis

from db.redis import redis_conn


class Settings(BaseModel):
    authjwt_secret_key: str = "secret"
    authjwt_denylist_enabled: bool = True
    authjwt_denylist_token_checks: set = {"access"}
    access_expires: int = timedelta(minutes=30)


settings = Settings()

# redis_conn = Redis(host='localhost', port=6379, db=0, decode_responses=True)


# callback to get configuration
@AuthJWT.load_config
def get_config():
    return settings


@AuthJWT.token_in_denylist_loader
def check_if_token_in_denylist(decrypted_token):
    jti = decrypted_token['jti']
    entry = redis_conn.get(jti)
    return entry and entry == 'true'
