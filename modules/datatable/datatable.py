import re
from enum import Enum
from typing import List, Any
from bson import regex
from pydantic import Field

from models.helpers.utils import RWModel


class TypeEnum(str, Enum):
    asc = 'asc'
    desc = 'desc'


class Order(RWModel):
    column: int
    dir: TypeEnum

    def get_order(self, column_name):
        if column_name[self.column].orderable:
            column_order_name = column_name[self.column].data
            column_order = -1
            if self.dir == "asc":
                column_order = 1
            order = {column_order_name: column_order}
            return order
        return None


class Search(RWModel):
    value: str = Field("")
    regex: bool

    def get_search_any(self, columns):
        if self.value != "":
            query_search_any = {"$or": []}
            if self.regex:
                # escape special character
                self.value = re.escape(self.value)
                search_by_regex = []
                for column in columns:
                    if column.searchable:
                        if column.name:
                            column_names = column.name.split(",")
                            for col_name in column_names:
                                search = {column.data + "." + col_name: {
                                    "$regex": regex.Regex(self.value), "$options": "-i"}}
                                search_by_regex.append(search)
                        else:
                            search = {column.data: {"$regex": regex.Regex(self.value), "$options": "-i"}}
                            search_by_regex.append(search)
                query_search_any["$or"] = search_by_regex
            else:
                search_by_word = []
                for column in columns:
                    if column.searchable:
                        if column.name:
                            column_names = column.name.split(",")
                            for col_name in column_names:
                                search = {column.data + "." + col_name: {"$eq": self.value}}
                                search_by_word.append(search)
                        else:
                            search = {column.data: {"$eq": self.value}}
                            search_by_word.append(search)
                query_search_any["$or"] = search_by_word
            return query_search_any
        return None


class Column(RWModel):
    data: str = Field(default="data_field_name",
                      description="for example: _id, activity, balance_before, balance_after, etc",
                      )
    name: str = Field("")
    searchable: bool
    orderable: bool
    search: Search

    def get_search_individual_column(self):
        if self.searchable and self.search.value != "":
            if self.search.regex:
                self.search.value = re.escape(self.search.value)
                if self.name:
                    names = self.name.split(",")
                    if len(names) > 1:
                        search = {"$or": []}
                        for name in names:
                            s = {self.data + "." + name: {
                                "$regex": regex.Regex(self.search.value), "$options": "-i"}}
                            search["$or"].append(s)
                    else:
                        search = {self.data + "." + names[0]: {
                            "$regex": regex.Regex(self.search.value), "$options": "-i"}}
                else:
                    search = {self.data: {"$regex": regex.Regex(self.search.value), "$options": "-i"}}
            else:
                if self.name:
                    names = self.name.split(",")
                    if len(names) > 1:
                        search = {"$or": []}
                        for name in names:
                            s = {self.data + "." + name: self.search.value}
                            search["$or"].append(s)
                    else:
                        search = {self.data + "." + names[0]: self.search.value}
                else:
                    search = {self.data: self.search.value}
            return search
        return None


class Datatable(RWModel):
    draw: int
    columns: List[Column]
    order: List[Order]
    start: int
    length: int = Field(10)
    search: Search

    def get_skip(self):
        if self.start is not None:
            return {"$skip": self.start}
        return None

    def get_limit(self):
        if self.length is not None and self.length > -1:
            return {"$limit": self.length}
        return None


class Filter(RWModel):
    field: str = Field("")
    values: List[Any]

    def get_filter(self):
        if self.field:
            values = {"$in": self.values}
            filter = {self.field: values}
            return filter
        else:
            return None


class Request(RWModel):
    datatable: Datatable
    filters: List[Filter]

    def query_search(self):
        query_search_all = {"$match": {}}

        # query search any
        search_any = self.datatable.search
        find_any = search_any.get_search_any(self.datatable.columns)

        # query individual search column
        query_search_individual_column = {"$or": []}
        search_individual_column = []
        search_columns = self.datatable.columns
        for search_column in search_columns:
            find_column = search_column.get_search_individual_column()
            if find_column is not None:
                search_individual_column.append(find_column)
        query_search_individual_column["$or"] = search_individual_column

        # query search all
        if find_any is not None:
            if len(query_search_individual_column["$or"]) > 0:
                query_search_all["$match"] = {"$and": [find_any, query_search_individual_column]}
            else:
                query_search_all["$match"] = find_any
        else:
            if len(query_search_individual_column["$or"]) > 0:
                query_search_all["$match"] = query_search_individual_column
        return query_search_all

    def query_sort(self):
        orders = self.datatable.order
        query_sort = {'$sort': {}}
        for order in orders:
            sort = order.get_order(self.datatable.columns)
            if sort is not None:
                query_sort["$sort"].update(sort)
        return query_sort

    def query_filter(self):
        query_filter = {"$match": {"$and": []}}
        filters = self.filters
        for filter in filters:
            filter = filter.get_filter()
            if filter is not None:
                query_filter["$match"]["$and"].append(filter)
        return query_filter

    def get_pipeline(self, query_count, query_data):
        pipeline = [{"$facet": {"count": query_count, "data": query_data}},
                    {"$addFields": {"recordsTotal": {"$size": "$count"}}},
                    {"$addFields": {"recordsFiltered": {"$size": "$data"}}},
                    {"$addFields": {"draw": self.datatable.draw}},
                    {"$project": {"draw": "$draw", "recordsTotal": "$recordsTotal",
                                  "recordsFiltered": "$recordsTotal",
                                  "data": "$data"}}]
        return pipeline

    def query_join(self, join):
        if join is not None:
            return join
        return None

    def generate_data_table(self, collection, datatable_schema, join=None):
        query_count = []
        query_data = []
        query_search = self.query_search()
        query_filter = self.query_filter()
        query_sort = self.query_sort()
        query_skip = self.datatable.get_skip()
        query_limit = self.datatable.get_limit()
        query_join = self.query_join(join)
        if query_join is not None:
            query_count += query_join
            query_data += query_join
        if len(query_search["$match"]) > 0:
            query_count.append(query_search)
            query_data.append(query_search)
        if len(query_filter["$match"]["$and"]) > 0:
            query_count.append(query_filter)
            query_data.append(query_filter)
        if len(query_sort["$sort"]) > 0:
            query_data.append(query_sort)
            query_count.append(query_sort)
        if query_skip is not None:
            query_data.append(query_skip)
        if query_limit is not None:
            query_data.append(query_limit)

        pipeline = self.get_pipeline(query_count, query_data)
        # print(pipeline)
        query = collection().dao.aggregate(pipeline)
        process = []
        if query is not None:
            for a in query:
                process.append(datatable_schema(**a).dict())
        return process[0]
