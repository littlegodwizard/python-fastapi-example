from bson import ObjectId
from fastapi import HTTPException
from starlette import status
from starlette.status import HTTP_400_BAD_REQUEST

from db.errors import EntityDoesNotExist
from models.helpers.utils import BSONObjectID
from models.user_balance_history import TypeEnum
from models.users import UserDao, UserInDB, User, UserWithBalance
from resources import strings
from fastapi_jwt_auth import AuthJWT
from services.user_balance import set_default_user_balance, add_transaction


async def check_username_is_taken(username: str) -> bool:
    try:
        await UserDao().get_user_by_username(username=username)
    except EntityDoesNotExist:
        return False

    return True


async def check_email_is_taken(email: str) -> bool:
    try:
        await UserDao().get_user_by_email(email=email)
    except EntityDoesNotExist:
        return False

    return True


async def create_user(
        user_agent: str,
        *,
        username: str,
        email: str,
        password: str,
) -> UserInDB:
    user = UserInDB(username=username, email=email)
    user.change_password(password)
    user.set_date()

    user_id = UserDao().create_new_user(user)

    # adding starting balance
    await set_default_user_balance(user_agent, user_id)

    return user.copy(update={"id": user_id})


async def get_current_user(authorization: AuthJWT):
    user_id = authorization.get_jwt_subject()
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    user = UserDao().get_user_by_id(BSONObjectID().validate(user_id))
    if user is None:
        raise credentials_exception
    return user


async def top_up_process(
        user_agent: str,
        *,
        nominal: float,
        user_id: BSONObjectID,
        username: str
):
    await add_transaction(
        user_id=user_id,
        input=nominal,
        type=TypeEnum.debit,
        activity="top-up",
        author=username,
        user_agent=user_agent
    )


async def transfer_process(
        user_agent: str,
        *,
        nominal: float,
        email_target: str,
        user_id: BSONObjectID,
        username: str
):
    incorrect_target_user = HTTPException(
        status_code=HTTP_400_BAD_REQUEST,
        detail=strings.USER_NOT_EXIST.format(email_target),
    )

    self_account = HTTPException(
        status_code=HTTP_400_BAD_REQUEST,
        detail=strings.CANNOT_TRANSFER_TO_YOURSELF,
    )

    # get user by email
    try:
        target_user = await UserDao().get_user_by_email(email=email_target)
    except EntityDoesNotExist as existence_error:
        raise incorrect_target_user from existence_error

    if target_user.id == user_id:
        raise self_account

    # sender side
    await add_transaction(
        user_id=user_id,
        input=nominal,
        type=TypeEnum.credit,
        activity="transfer",
        author=username,
        user_agent=user_agent
    )

    # receipt side
    await add_transaction(
        user_id=target_user.id,
        input=nominal,
        type=TypeEnum.debit,
        activity="transfer",
        author=username,
        user_agent=user_agent
    )
