from typing import List

from models.helpers.utils import BSONObjectID, RWModel
from models.user_balance_history import UserBalanceHistoryInDB, TypeEnum, UserBalanceHistoryDao


class DataTableUserBalanceHistory(RWModel):
    draw: int
    recordsTotal: int
    recordsFiltered: int
    data: List[UserBalanceHistoryInDB]


async def add_user_balance_history(
        *,
        balance_before: float,
        balance_after: float,
        activity: str,
        type: TypeEnum,
        user_agent: str,
        author: str,
        user_balance_id: BSONObjectID,
) -> UserBalanceHistoryInDB:
    user_balance_history = UserBalanceHistoryInDB(balance_before=balance_before,
                                                  balance_after=balance_after,
                                                  activity=activity,
                                                  type=type,
                                                  # ip=ip,
                                                  # location=location,
                                                  user_agent=user_agent,
                                                  author=author,
                                                  user_balance_id=user_balance_id)
    user_balance_history.set_date()
    user_balance_history.set_loc()
    user_balance_history_id = UserBalanceHistoryDao().add_user_balance_history(user_balance_history)

    return user_balance_history.copy(update={"id": str(user_balance_history_id)})


def pipeline_datatable(user_id: BSONObjectID):
    pipeline = [
        {"$lookup": {
            "from": "user_balance",
            "let": {"user_balance_id": "$user_balance_id"},
            "pipeline": [
                {"$match": {"$expr": {"$eq": ["$$user_balance_id", "$_id"]}}}
            ],
            "as": "balance_total"
        }},
        {"$addFields": {
            "balance_total": {"$arrayElemAt": ["$balance_total", 0]}
        }},

        {"$lookup": {
            "from": "users",
            "let": {"user_id": "$balance_total.user_id"},
            "pipeline": [
                {"$match": {"$expr": {"$eq": ["$$user_id", "$_id"]}}}
            ],
            "as": "user"
        }},
        {"$addFields": {
            "user": {"$arrayElemAt": ["$user", 0]}
        }},
        {"$match": {"$expr": {"$eq": ["$user._id", user_id]}}}
    ]
    return pipeline


async def show_user_balance_history(request, user):
    return request.generate_data_table(UserBalanceHistoryDao, DataTableUserBalanceHistory,
                                       pipeline_datatable(BSONObjectID().validate(user.id)))
