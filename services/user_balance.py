import operator
from typing import Optional

from fastapi import HTTPException
from starlette.status import HTTP_400_BAD_REQUEST

from models.helpers.utils import BSONObjectID, RWModel
from models.user_balance import UserBalanceInDB, UserBalanceDao
from models.user_balance_history import TypeEnum
from resources import strings
from services.user_balance_history import add_user_balance_history


class BalanceCalculation(RWModel):
    user_id: BSONObjectID
    input: float

    def check_saldo(self, balance_before):
        if balance_before < self.input:
            return False
        return True

    async def calculate_balance(self, type_transaction):
        balance_now = UserBalanceDao().get_by_user(user_id=self.user_id)
        balance_before = float(0)
        if balance_now is not None:
            balance_before = balance_now.balance_achieve

        if type_transaction == TypeEnum.debit:
            balance_after = operator.add(balance_before, self.input)

        else:
            # check balance
            if not self.check_saldo(balance_before):
                raise HTTPException(
                    status_code=HTTP_400_BAD_REQUEST,
                    detail=strings.INSUFFICIENT_BALANCE,
                )
            else:
                balance_after = operator.sub(balance_before, self.input)
                # print(balance_after)
        return balance_now, balance_before, balance_after


async def add_user_balance(
        *,
        # balance: str,
        balance_achieve: float,
        user_id: BSONObjectID,
) -> UserBalanceInDB:
    user_balance = UserBalanceInDB(balance_achieve=balance_achieve, user_id=user_id)
    user_balance.set_date()
    user_balance_id = UserBalanceDao().save_user_balance(user_balance)
    return user_balance.copy(update={"id": user_balance_id})


async def add_transaction(
        *,
        user_id: BSONObjectID,
        input: float,
        # ip: Optional[str] = None,
        user_agent: Optional[str] = None,
        # location: Optional[str] = None,
        type: TypeEnum,
        activity: str,
        author: str
):
    # calculate balance
    balance_calculation = BalanceCalculation(user_id=user_id, input=input)
    balance_now, balance_before, balance_after = await balance_calculation.calculate_balance(type)

    # update balance
    user_balance = await add_user_balance(balance_achieve=balance_after, user_id=user_id)

    # completing info

    # update balance history
    user_balance_history = await add_user_balance_history(
        balance_before=balance_before,
        balance_after=balance_after,
        activity=activity,
        type=type,
        # ip=ip,
        # location=location,
        user_agent=user_agent,
        author=author,
        user_balance_id=user_balance.id if balance_now is None else balance_now.id
    )

    return user_balance_history


async def set_default_user_balance(user_agent, user_id: BSONObjectID):
    await add_transaction(
        user_id=user_id,
        input=1000,
        type=TypeEnum.debit,
        activity="register",
        author="system",
        user_agent=user_agent
    )