from typing import Optional

from bson import ObjectId
from fastapi import Depends
from pymongo.collection import Collection

from db.mongodb import get_collection
from models.helpers.mixin import IDModelMixin, DateTimeModelMixin
from models.helpers.utils import RWModel, BSONObjectID


class UserBalance(RWModel):
    balance: Optional[str] = ""
    balance_achieve: float


class UserBalanceInDB(IDModelMixin, DateTimeModelMixin, UserBalance):
    user_id: BSONObjectID


class UserBalanceDao:
    def __init__(self):
        self.dao: Collection = Depends(get_collection("user_balance")).dependency

    def save_user_balance(self, user_balance: UserBalanceInDB):
        print(user_balance.id)
        save = self.dao.update_one({"user_id": user_balance.user_id}, {"$set": user_balance.dict()}, upsert=True)
        upserted_id = save.upserted_id
        if upserted_id is not None:
            return str(upserted_id)
        else:
            return str(user_balance.id)

    def get_by_user(self, user_id):
        userBalance = self.dao.find_one({"user_id": user_id})
        if userBalance is not None:
            return UserBalanceInDB(**userBalance)
        return None
