import datetime
import json
from urllib.request import urlopen

from pydantic import BaseModel, Field, validator

from models.helpers.utils import BSONObjectID, RWModel
from starlette.requests import Request as Req


class DateTimeModelMixin(BaseModel):
    created_at: datetime.datetime = None  # type: ignore
    updated_at: datetime.datetime = None  # type: ignore

    @validator("created_at", "updated_at", pre=True)
    def default_datetime(
            cls,  # noqa: N805
            value: datetime.datetime,  # noqa: WPS110
    ) -> datetime.datetime:
        return value or datetime.datetime.now()

    def set_date(self):
        self.updated_at = self.default_datetime(self.updated_at)
        self.created_at = self.default_datetime(self.created_at)
        # if self.created_at is None:
        #     self.created_at = datetime.datetime.now()
        # self.updated_at = datetime.datetime.now()


class IDModelMixin(BaseModel):
    id: BSONObjectID = Field(None, alias='_id')


class LocationModelMixin(RWModel):
    ip: str = None
    location: str = None

    def set_loc(self):
        url = 'http://ipinfo.io/json'
        response = urlopen(url)
        data = json.load(response)

        self.ip = data['ip']
        self.location = data['loc']
